import e621
import db
import uitext
import eyes
import keys

import sys
import logging
from telegram import ParseMode, error

# Enables logging
logger = logging.getLogger(__name__)

def discardMessage(update, allow_stopped_users = False, admin_only = False):
    """
    Filters out cases where a handler might fail or produce unexpected output by discarding erroneous messages
    """
    # Discards edited messages
    if update.message == None:
        logger.debug("Discarded edited message")
        return True

    # Discards messages from the stopped users list, but sends prompt in case they forgot how to restart the bot
    if not allow_stopped_users:
        if update.message.chat.id in eyes.stoppedUsers:
            logger.debug("stopped user " + str(update.message.chat.id) + " sent: " + update.message.text)
            update.message.reply_text(uitext.oops_stopped, parse_mode = ParseMode.HTML)
            return True

    # Discards messages from non-admins if desired
    if admin_only:
        db_conn = db.Connection(keys.database_key)
        if not db_conn.isAdmin(update.message.chat.id):
            update.message.reply_text(uitext.oops_not_admin, parse_mode = ParseMode.HTML)
            return True

    # Passed all checks
    return False

def sendMessageTooLong(update, messageContent):
    """
    Split a very large message into Telegram-acceptable chunks (produces ugly results, too bad)
    """
    messageChunks = [messageContent[i:i+4096] for i in range(0, len(messageContent), 4096)]
    for chunk in messageChunks:
        update.message.reply_text(chunk) # No parsing because html tags break across chunks

def start(update, context, updater):
    """
    Starts the bot for the user and sends a welcome message
    """
    if discardMessage(update, allow_stopped_users = True):
        return

    db_conn = db.Connection(keys.database_key)
    messageContent = db_conn.createNewUser(update.message.chat.id)

    # Removes user from in-memory list of stopped users
    if update.message.chat.id in eyes.stoppedUsers:
        eyes.stoppedUsers.remove(update.message.chat.id)

    update.message.reply_text(uitext.start, parse_mode = ParseMode.HTML)

    # Notifies administrators of new users and un-stopped users
    if messageContent:
        for chatID in db_conn.getAllAdmins():
            message = updater.bot.send_message(chatID, messageContent, parse_mode = ParseMode.HTML)

def stop(update, context):
    """
    Asks the user to confirm that they want to delete all of their data
    """
    if discardMessage(update):
        return

    update.message.reply_text(uitext.stop, parse_mode = ParseMode.HTML)

def stop_confirm(update, context, updater):
    """
    Deletes the user
    """
    if discardMessage(update):
        return

    db_conn = db.Connection(keys.database_key)
    db_conn.stopUser(update.message.chat.id)
    eyes.stoppedUsers.append(update.message.chat.id)

    update.message.reply_text(uitext.stop_confirm, parse_mode = ParseMode.HTML)

    # Notifies administrators of stopped user
    for chatID in db_conn.getAllAdmins():
        message = updater.bot.send_message(chatID, "⏹ A user has stopped the bot", parse_mode = ParseMode.HTML)

def help(update, context):
    """
    Displays helpful details about command usage
    """
    if discardMessage(update):
        return

    update.message.reply_text(uitext.help, parse_mode = ParseMode.HTML, disable_web_page_preview = True)

def faq(update, context):
    """
    Answers frequently asked questions
    """
    if discardMessage(update):
        return

    update.message.reply_text(uitext.faq, parse_mode = ParseMode.HTML, disable_web_page_preview = True)

def maxrating(update, context):
    """
    Helps the user change their maximum rating preference
    """
    if discardMessage(update):
        return

    update.message.reply_text(uitext.maxrating, parse_mode = ParseMode.HTML)

def setmaxrating(update, context, rating):
    """
    Sets the user's maximum rating
    """
    if discardMessage(update):
        return

    db_conn = db.Connection(keys.database_key)
    db_conn.updateUserMaxRating(update.message.chat.id, rating)
    update.message.reply_text(uitext.maxrating_success, parse_mode = ParseMode.HTML)

def showtags(update, context):
    """
    Displays the user's watched tags list and blacklist
    """
    if discardMessage(update):
        return

    db_conn = db.Connection(keys.database_key)
    watchedTags, incidentalTags, blacklistedTags = db_conn.getUserTags(update.message.chat.id)

    # Alphabetizes the lists
    watchedTags.sort()
    blacklistedTags.sort()

    # Adds the user's watched tags to the message
    messageContent = "<b>Watched tags:</b>"
    if watchedTags == []:
        messageContent += " None"
    else:
        for tag in watchedTags:
            messageContent += " " + tag

    # Adds the user's incidental tags to the message
    messageContent += "\n\n<b>Incidental tags:</b>"
    if incidentalTags == []:
        messageContent += " None"
    else:
        for tag in incidentalTags:
            messageContent += " " + tag

    # Adds the user's blacklisted tags to the message
    messageContent += "\n\n<b>Blacklisted tags:</b>"
    if blacklistedTags == []:
        messageContent += " None"
    else:
        for tag in blacklistedTags:
            messageContent += " " + tag
    try:
        update.message.reply_text(messageContent, parse_mode = ParseMode.HTML)
    except error.BadRequest as updateError:
        if str(updateError) == "Message is too long":
            # Break up messages into smaller chunks
            sendMessageTooLong(update, messageContent)
        else:
            logger.error("Error when trying to send list of tags to user: " + str(sys.exc_info()[0]))

def cleanTags(tags):
    """
    Cleans up user supplied tags and turns them into a list
    """
    # Makes all tags lowercase (e621 tags are case-insensitive)
    tags = tags.lower()
    
    # Removes Telegram command from string
    try:
        tags = tags.split(" ", 1)[1]
    except IndexError:
        return None

    # Removes characters that e621 refuses
    tags = tags.replace("%", "").replace(",", "").replace("#", "").replace("\\", "").replace("*", "")
    
    # Removes characters that break Telegram's html parsing. This makes a small number of tags unfollowable, too bad
    tags = tags.replace("<", "").replace(">", "")

    if tags == "":
        # No tags were specified
        return None

    # Splits the tags into a list
    tags = tags.split(" ")

    cleanTagsList = []
    for tag in tags:
        if tag == "":
            # Ignores erroneous spaces
            continue
        if tag[0] == "-" or tag[0] == "~":
            # Removes e621-prohibited initial characters
            tag = tag[1:]
        if not tag == "":
            # Adds acceptable tag to final list
            cleanTagsList.append(tag)

    return cleanTagsList

def addtagstolist(update, context, specified_list):
    """
    Adds the given tags to the specified list
    """
    if discardMessage(update):
        return

    tags = cleanTags(update.message.text)

    # Checks if the user didn't specify any tags
    if tags == None:
        update.message.reply_text(uitext.oops_no_tags, parse_mode = ParseMode.HTML)
        return

    # Pacifies the user in case it takes a while
    update.message.reply_text(uitext.processing, parse_mode = ParseMode.HTML)

    db_conn = db.Connection(keys.database_key)

    # Assigns tag lists depending on which one the user is adding to
    if specified_list == "watchlist":
        specifiedTags, otherTags1, otherTags2 = db_conn.getUserTags(update.message.chat.id)
        other_list1 = "incidentals"
        other_list2 = "blacklist"
    elif specified_list == "incidentals":
        otherTags1, specifiedTags, otherTags2 = db_conn.getUserTags(update.message.chat.id)
        other_list1 = "watchlist"
        other_list2 = "blacklist"
    elif specified_list == "blacklist":
        otherTags1, otherTags2, specifiedTags = db_conn.getUserTags(update.message.chat.id)
        other_list1 = "watchlist"
        other_list2 = "incidentals"

    messageContent = "<b>📃 Transaction Report</b>\n\n"
    acceptedTags = []
    ambiguousFlag = False
    errorFlag = False

    # Checks tags and assembles the transaction report simultaneously
    for tag in tags:
        messageContent += "- <b>" + tag + "</b> "

        # Checks if tag is already in specified list or if it has already been processed
        if tag in specifiedTags or tag in acceptedTags:
            messageContent += "❌ Tag already in " + specified_list + "\n"
            continue
        # Checks if tag is in other tags list
        elif tag in otherTags1:
            messageContent += "❌ Tag is in " + other_list1 + "; remove tag from " + other_list1 + " and try again\n"
            continue
        elif tag in otherTags2:
            messageContent += "❌ Tag is in " + other_list2 + "; remove tag from " + other_list2 + " and try again\n"
            continue

        try:
            isTag, alias = e621.isTag(tag)
        except:
            logger.error("Error when fetching tag: " + str(sys.exc_info()[1]))
            messageContent += "🚧 I'm sorry, I encountered an error while processing this tag.\n"
            errorFlag = True
            continue

        # Checks if tag is not valid e621 tag
        if not isTag or alias == "invalid_tag":
            messageContent += "❌ Not a valid e621 tag\n"
        else:
            # Checks if tag is aliased
            if not alias == None:
                messageContent += "➡️ <b>" + alias + "</b> "
                # Checks if alias is already in the user's specified tags list or if it has already been processed
                if alias in specifiedTags or alias in acceptedTags:
                    messageContent += "❌ Alias already in " + specified_list + "\n"
                # Checks if alias is in user's other tags list
                elif alias in otherTags1:
                    messageContent += "❌ Alias is in " + other_list1 + "; remove tag from " + other_list1 + " and try again\n"
                elif alias in otherTags2:
                    messageContent += "❌ Alias is in " + other_list2 + "; remove tag from " + other_list2 + " and try again\n"
                else:
                    acceptedTags.append(alias)
                    # Checks if ambiguous tag
                    if "disambiguation" in alias:
                        messageContent += "⚠️ Ambiguous tag added to " + specified_list + ".\n"
                        ambiguousFlag = True
                    else:
                        messageContent += "✔️ Alias added to " + specified_list + "\n"
            else:
                acceptedTags.append(tag)
                messageContent += "✔️ Tag added to " + specified_list + "\n"

    if ambiguousFlag:
        messageContent += "\n⚠️ I've added at least one ambiguous tag to your " + specified_list + ". "
        messageContent += "These are tags that e621 considers to be too vague. For best results, please add additional, more specific tags."

    if errorFlag:
        messageContent += uitext.at_least_one_error

    # Adds the accepted tags to the user's specified tags list
    if specified_list == "watchlist":
        db_conn.addWatchedTags(update.message.chat.id, acceptedTags)
    elif specified_list == "blacklist":
        db_conn.addBlacklistedTags(update.message.chat.id, acceptedTags)
    elif specified_list == "incidentals":
        db_conn.addIncidentalTags(update.message.chat.id, acceptedTags)

    try:
        update.message.reply_text(messageContent, parse_mode = ParseMode.HTML)
    except error.BadRequest as updateError:
        if str(updateError) == "Message is too long":
            # Break up messages into smaller chunks
            sendMessageTooLong(update, messageContent)
        else:
            logger.error("Error when trying to send added tags transaction report to user: " + str(sys.exc_info()[0]))

def removetagsfromlist(update, context, specified_list):
    """
    Removes the given tags from the specified list
    """
    if discardMessage(update):
        return

    tags = cleanTags(update.message.text)

    # Checks if the user didn't specify any tags
    if tags == None:
        update.message.reply_text(uitext.oops_no_tags, parse_mode = ParseMode.HTML)
        return

    # Pacifies the user in case it takes a while
    update.message.reply_text(uitext.processing, parse_mode = ParseMode.HTML)

    db_conn = db.Connection(keys.database_key)

    # Assigns tag lists depending on which one the user is adding to
    if specified_list == "watchlist":
        specifiedTags, otherTags1, otherTags2 = db_conn.getUserTags(update.message.chat.id)
    elif specified_list == "incidentals":
        otherTags1, specifiedTags, otherTags2 = db_conn.getUserTags(update.message.chat.id)
    elif specified_list == "blacklist":
        otherTags1, otherTags2, specifiedTags = db_conn.getUserTags(update.message.chat.id)

    messageContent = "<b>📃 Transaction Report</b>\n\n"
    acceptedTags = []

    # Checks tags and assembles the transaction report simultaneously
    for tag in tags:
        messageContent += "- <b>" + tag + "</b> "

        # Checks if tag is aliased
        try:
            isTag, alias = e621.isTag(tag)
        except:
            logger.error("Error when fetching tag: " + str(sys.exc_info()[1]))
            messageContent += "🚧 I'm sorry, I encountered an error while processing this tag.\n"
            errorFlag = True
            continue
        if not alias == None:
            tag = alias
            messageContent += "➡️ <b>" + tag + "</b> "
        
        # Checks if tag has already been specified
        if tag in acceptedTags:
            messageContent += "❌ Tag already in list\n"
            continue
        # Checks if tag is in user's specified list
        if tag in specifiedTags:
            acceptedTags.append(tag)
            messageContent += "✔️ Tag removed from " + specified_list + "\n"
        else:
            messageContent += "❌ Tag not in " + specified_list + "\n"

    # Removes the accepted tags from the user's specified tags list
    if specified_list == "watchlist":
        db_conn.removeWatchedTags(update.message.chat.id, acceptedTags)
    elif specified_list == "blacklist":
        db_conn.removeBlacklistedTags(update.message.chat.id, acceptedTags)
    elif specified_list == "incidentals":
        db_conn.removeIncidentalTags(update.message.chat.id, acceptedTags)

    try:
        update.message.reply_text(messageContent, parse_mode = ParseMode.HTML)
    except error.BadRequest as updateError:
        if str(updateError) == "Message is too long":
            # Break up messages into smaller chunks
            sendMessageTooLong(update, messageContent)
        else:
            logger.error("Error when trying to send removed tags transaction report to user: " + str(sys.exc_info()[0]))

def showpools(update, context):
    """
    Displays the user's watched pools lists
    """
    if discardMessage(update):
        return

    db_conn = db.Connection(keys.database_key)
    watchedPools = db_conn.getUserPools(update.message.chat.id)

    # Alphabetizes the list
    watchedPools.sort()
    
    messageContent = "<b>Watched pools:</b> "
    if watchedPools == []:
        messageContent += "None"
    else:
        for poolID in watchedPools:
            messageContent += str(poolID) + " "

    try:
        update.message.reply_text(messageContent, parse_mode = ParseMode.HTML)
    except error.BadRequest as updateError:
        if str(updateError) == "Message is too long":
            # Break up messages into smaller chunks
            sendMessageTooLong(update, messageContent)
        else:
            logger.error("Error when trying to send list of pools to user: " + str(sys.exc_info()[0]))

def cleanPools(pools):
    """
    Cleans up user supplied poolIDs and turns them into a list.
    """
    # Removes Telegram command from string
    try:
        pools = pools.split(" ", 1)[1]
    except IndexError:
        return None

    # Removes characters that e621 refuses
    pools = pools.replace("%", "").replace(",", "").replace("#", "").replace("\\", "").replace("*", "")
    
    # Removes characters that break Telegram's html parsing.
    pools = pools.replace("<", "").replace(">", "")

    if pools == "":
        # No pools were specified
        return None

    # Splits the pools into a list
    pools = pools.split(" ")
    cleanPoolsList = []
    for poolID in pools:
        if poolID == "":
            # Ignores erroneous spaces
            continue
        cleanPoolsList.append(poolID)

    return cleanPoolsList

def addpools(update, context):
    """
    Adds the specified pools to the user's watched pools list
    """
    if discardMessage(update):
        return

    pools = cleanPools(update.message.text)

    # Checks if the user didn't specify any pools
    if pools == None:
        update.message.reply_text(uitext.oops_no_pools, parse_mode = ParseMode.HTML)
        return

    # Pacifies the user in case it takes a while
    update.message.reply_text(uitext.processing, parse_mode = ParseMode.HTML)

    db_conn = db.Connection(keys.database_key)
    watchedPools = db_conn.getUserPools(update.message.chat.id)
    # Makes the list of pools a list of strings instead of integers
    watchedPools = list(map(str, watchedPools))

    messageContent = "<b>📃 Transaction Report</b>\n\n"
    acceptedPools = []
    errorFlag = False

    # Checks pools and assembles the transaction report simultaneously
    for poolID in pools:
        messageContent += "- <b>" + str(poolID) + "</b> "

        # Checks if pool is already in the user's list of watched pools or if it has already been processed
        if poolID in watchedPools or poolID in acceptedPools:
            messageContent += "❌ Pool already in list\n"
            continue

        try:
            isPool = e621.isPool(poolID)
        except:
            logger.error("Error when fetching pool: " + str(sys.exc_info()[1]))
            messageContent += "🚧 I'm sorry, I encountered an error while processing this pool ID.\n"
            errorFlag = True
            continue

        # Checks if pool is not valid e621 pool
        if not isPool:
            messageContent += "❌ Not a valid e621 pool\n"
        else:
            acceptedPools.append(poolID)
            messageContent += "✔️ Pool added to watchlist\n"

    # Adds the accepted pools to the user's watched pools list
    db_conn.addWatchedPools(update.message.chat.id, acceptedPools)

    if errorFlag:
        messageContent += uitext.at_least_one_error

    try:
        update.message.reply_text(messageContent, parse_mode = ParseMode.HTML)
    except error.BadRequest as updateError:
        if str(updateError) == "Message is too long":
            # Break up messages into smaller chunks
            sendMessageTooLong(update, messageContent)
        else:
            logger.error("Error when trying to send added pools transaction report to user: " + str(sys.exc_info()[0]))

def rempools(update, context):
    """
    Removes the specified pools from the user's watched pools list
    """
    if discardMessage(update):
        return

    pools = cleanPools(update.message.text)

    # Checks if the user didn't specify any pools
    if pools == None:
        update.message.reply_text(uitext.oops_no_pools, parse_mode = ParseMode.HTML)
        return

    # Pacifies the user in case it takes a while
    update.message.reply_text(uitext.processing, parse_mode = ParseMode.HTML)

    db_conn = db.Connection(keys.database_key)
    watchedPools = db_conn.getUserPools(update.message.chat.id)
    # Makes the list of pools a list of strings instead of integers
    watchedPools = list(map(str, watchedPools))

    messageContent = "<b>📃 Transaction Report</b>\n\n"
    acceptedPools = []

    # Checks pools and assembles the transaction report simultaneously
    for poolID in pools:
        messageContent += "- <b>" + str(poolID) + "</b> "

        # Checks if pool has already been specified
        if poolID in acceptedPools:
            messageContent += "❌ Pool already in list\n"
            continue
        # Checks if pool is in user's list of watched pools
        if poolID in watchedPools:
            acceptedPools.append(poolID)
            messageContent += "✔️ Pool removed from watchlist\n"
        else:
            messageContent += "❌ Pool not in watchlist\n"

    # Removes the accepted pools from the user's watched pools list
    db_conn.removeWatchedPools(update.message.chat.id, acceptedPools)

    try:
        update.message.reply_text(messageContent, parse_mode = ParseMode.HTML)
    except error.BadRequest as updateError:
        if str(updateError) == "Message is too long":
            # Break up messages into smaller chunks
            sendMessageTooLong(update, messageContent)
        else:
            logger.error("Error when trying to send removed pools transaction report to user: " + str(sys.exc_info()[0]))

def testcon(update, context):
    """
    Tests to see if the e621 API is reachable
    """
    if discardMessage(update):
        return

    # Runs a simple test query
    try:
        test = e621.getPosts([], 1, 1)
    except:
        # Reports that something went wrong
        logger.error("Error when testing API connection: " + str(sys.exc_info()[1]))
        update.message.reply_text("❌ I failed to reach the e621 API: " + str(sys.exc_info()[1]))
    else:
        # Reports that nothing went wrong
        update.message.reply_text("✔️ I was able to reach the e621 API")

def cancel(update, context):
    """
    A placebo, prevents users from thinking that they can't back out of a dialog
    """
    if discardMessage(update):
        return

    update.message.reply_text(uitext.cancel, parse_mode = ParseMode.HTML)

def announce(update, context, updater):
    """
    Blasts out an announcement to all active users
    """
    if discardMessage(update, admin_only = True):
        return

    # Adds header to message and strips out command
    messageContent = "📢 <b>Announcement</b>\n\n" + update.message.text_html[10:] + "\n\n-@" + update.message.from_user.username

    db_conn = db.Connection(keys.database_key)
    for chatID in db_conn.getActiveUsers():
        try:
            message = updater.bot.send_message(chatID, messageContent, parse_mode = ParseMode.HTML)
            logger.debug("Sent announcement to " + str(chatID))
        except error.BadRequest:
            logger.debug("Failed to send message to user " + str(chatID))
            # User couldn't be reached, but other parts of the code handle this scenario instead, so throws away the error
        except error.Unauthorized:
            logger.debug("Failed to send message to user " + str(chatID))
            # Same as above, throws it away

def userstats(update, context):
    """
    Retrieves some useful statistics
    """
    if discardMessage(update, admin_only = True):
        return

    db_conn = db.Connection(keys.database_key)
    messageContent = "<b>📃 Users Report</b>\n\n"
    messageContent += "<code>  Active users:  " + str(len(db_conn.getActiveUsers())) + "</code>\n"
    messageContent += "<code>+ Dormant users: " + str(len(db_conn.getDormantUsers())) + "</code>\n"
    messageContent += "<code>+ Stopped users: " + str(len(db_conn.getStoppedUsers())) + "</code>\n"
    messageContent += "<code>= TOTAL:         " + str(len(db_conn.getAllUsers())) + "</code>\n"
    
    update.message.reply_text(messageContent, parse_mode = ParseMode.HTML)

def promote(update, context):
    """
    Promotes a user to an admin
    """
    if discardMessage(update, admin_only = True):
        return

    # Promotes the specified user
    db_conn = db.Connection(keys.database_key)
    db_conn.promoteUser(update.message.text[9:])

    if db_conn.isAdmin(update.message.text[9:]):
        update.message.reply_text("✔️ Promoted " + update.message.text[9:], parse_mode = ParseMode.HTML)
    else:
        update.message.reply_text("❌ Failed to promote " + update.message.text[9:], parse_mode = ParseMode.HTML)

def demote(update, context):
    """
    Demotes an admin to a user
    """
    if discardMessage(update, admin_only = True):
        return

    # Disallows self-demotion to prevent a lock-out
    if update.message.text[8:] == update.message.chat.id:
        update.message.reply_text("❌ You cannnot demote yourself", parse_mode = ParseMode.HTML)
        return

    # Demotes the specified user
    db_conn = db.Connection(keys.database_key)
    db_conn.demoteUser(update.message.text[8:])

    if db_conn.isAdmin(update.message.text[8:]):
        update.message.reply_text("❌ Failed to demote " + update.message.text[8:], parse_mode = ParseMode.HTML)
    else:
        update.message.reply_text("✔️ Demoted " + update.message.text[8:], parse_mode = ParseMode.HTML)

def showadmins(update, context):
    """
    Provides a list of admins
    """
    if discardMessage(update, admin_only = True):
        return

    db_conn = db.Connection(keys.database_key)
    messageContent = "<b>📃 Admins Report</b>\n\n"
    for chatID in db_conn.getAllAdmins():
        messageContent += "<code>" + str(chatID) + "</code>\n"

    update.message.reply_text(messageContent, parse_mode = ParseMode.HTML)

def suspend(update, context):
    """
    Puts bot on hold
    """
    if discardMessage(update, admin_only = True):
        return

    # Suspends the bot
    db_conn = db.Connection(keys.database_key)
    db_conn.suspend()

    update.message.reply_text("✔️ Bot suspended", parse_mode = ParseMode.HTML)

def unsuspend(update, context):
    """
    Lets the bot run
    """
    if discardMessage(update, admin_only = True):
        return

    # Suspends the bot
    db_conn = db.Connection(keys.database_key)
    db_conn.unsuspend()

    update.message.reply_text("✔️ Bot unsuspended", parse_mode = ParseMode.HTML)

def noncommand(update, context):
    """
    Defines noncommand behavior
    """
    return
