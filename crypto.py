# AES 256 encryption/decryption using pycrypto library
# https://medium.com/qvault/aes-256-cipher-python-cryptography-examples-b877b9d2e45e
import secrets
import string
import base64
import hashlib
from Cryptodome.Cipher import AES
from Cryptodome.Random import get_random_bytes

def generateDatabaseKey():
    alphabet = string.ascii_letters + string.digits
    database_key = "".join(secrets.choice(alphabet) for i in range(32))
    return database_key

def encrypt(password, plain_text):
    """
    Encrypts the supplied text using AES-256-CBC (PBE) into raw data blobs
    """
    # Generates a random salt and a randoom initialization vector
    salt = get_random_bytes(AES.block_size)
    iv = get_random_bytes(AES.block_size)

    # Generates a private key from the password and salt
    private_key = hashlib.scrypt(password.encode(), salt=salt, n=2**14, r=8, p=1, dklen=32)

    # Padds text with spaces so the input is valid
    padded_text = plain_text + (AES.block_size - (len(plain_text) % AES.block_size)) * " "

    # Creates the cypher configuration
    cipher_config = AES.new(private_key, AES.MODE_CBC, iv)

    # Encrypts the text
    cipher_text = cipher_config.encrypt(padded_text.encode("utf-8"))

    # Returns the data
    return [base64.b64encode(cipher_text), base64.b64encode(salt), base64.b64encode(iv)]

def decrypt(password, List):
    """
    Decrypts supplied raw data using AES-256-CBC (PBE)
    """
    cipher_text = base64.b64decode(List[0])
    salt = base64.b64decode(List[1])
    iv = base64.b64decode(List[2])

    # Generates the private key from the password and salt
    private_key = hashlib.scrypt(password.encode(), salt=salt, n=2**14, r=8, p=1, dklen=32)

    # Creates the cypher configuration
    cipher_config = AES.new(private_key, AES.MODE_CBC, iv)

    # Decrypt the ciphered text
    plain_text = cipher_config.decrypt(cipher_text)

    # Returns the original text
    return plain_text.rstrip().decode("utf-8")
