# Eyes on e621

![](icon-tiny.png)

My official instance: [@eyes_on_e621_bot](https://t.me/eyes_on_e621_bot)

A [Telegram](https://telegram.org/) bot that allows users to subscribe to content from [e621 (NSFW link!)](https://e621.net/).

## Features

### User features
* Follow an unlimited number of tags and pools
* Blacklist an unlimited number of tags
* Know when incidental tags appear on posts
* Define a maximum rating: safe, questionable, or explicit
* Abundance of useful metadata attached to received posts
* See exactly which tags matched your watchlist
* Convenient Telegram-generated preview with link to original file
* Easy to delete your data and stop all subscriptions

### Administration features
* Very lightweight
* Short list of dependencies
* Integrated SQLite database
* Easy setup script kicks in the first time you run the bot
* Keys and database encrypted, unlocked with password at runtime
* Blast out announcements without leaving Telegram

## Setup

### Accounts

#### Telegram

You will need a [Telegram](https://telegram.org/) account.

Create a new bot by talking to [@BotFather](https://t.me/BotFather) on Telegram. Note your API token. The table below contains the recommended default settings.

| Setting | Recommended Default |
| ------------ | ------------ |
| Name | Eyes on e621 |
| Description and About |  Never miss a post! Subscribe to any number of tags and get new e621 content delivered directly to you. |
| Botpic | (Upload  icon-black.png) |
| Commands | help - Show the help dialog<br>showtags - Show your tags<br>addtags - Add tags to watchlist<br>remtags - Remove tags from watchlist<br>addincs - Add tags to incidentals<br>remincs - Remove tags from incidentals<br>addbl - Add tags to blacklist<br>rembl - Remove tags from blacklist<br>showpools - Show your pools<br>addpools - Add pools to watchlist<br>rempools - Remove pools from watchlist<br>maxrating - Set max allowed rating<br>testcon - Check if e621 is down<br>faq - Read the frequently asked questions |
| Inline mode | Disabled |
| Allow Groups | Disabled |

You will also need your Telegram chatID. If you don't know it, you can get it using [@userinfobot](https://t.me/userinfobot).

#### e621.net

You will need an e621.net account. It is recommended that you create a new account specifically for this bot. You will need to enable API access through your account settings. Note your API token.

### Local Environment

I wrote this software on Fedora and run it on Debian, but it should work on all platforms that support Python and the Python modules it depends on. Please note that the syntax included here is just for reference as it may be different for your distribution (for example, some distributions use `python3` instead of `python`).

Ensure that the following dependencies are installed. Use your preferred package manager.

* [python 3.x](https://www.python.org/)
* [python virtualenv](https://docs.python.org/3/tutorial/venv.html)
* [git](https://git-scm.com/)

Next, create a Python virtual environment in the directory where you want to run the bot from. You may wish to use [screen](https://linux.die.net/man/1/screen) or a similar terminal manager solution here, but it is not required.

```bash
$ cd /path/to/bot/
$ virtualenv -p python3 eyes
$ cd eyes
$ source bin/activate
```

Then, use pip to install some additional dependencies.

* [python-telegram-bot](https://python-telegram-bot.org/)
* [pycryptodomex](https://www.pycryptodome.org/en/latest/)
* [tldextract](https://github.com/john-kurkowski/tldextract)

```bash
(eyes) $ python -m pip install python-telegram-bot pycryptodomex tldextract
```

* [sqlcipher3](https://github.com/sqlcipher/sqlcipher) (Note the version of sqlcipher that you install. It is not compatible by default across versions. This may limit the portability of your install.)

If you are running the bot on a 64-bit machine, the easiest way to install sqlcipher is by using the sqlcipher3-binary package. This package is statically linked to the latest sqlcipher community release.

```bash
## 64-bit machines
(eyes) $ python -m pip install sqlcipher3-binary
```

If you are running the bot on an ARM machine, it is a bit more complicated

```bash
## ARM machines
## First, install or compile sqlcipher and the python 3.x dev tools. The process will depend on your distribution and whether or not binaries are available.
(eyes) $ python -m pip install wheel
(eyes) $ python -m pip install sqlcipher3
```

Finally, download the code

```bash
(eyes) $ git clone https://gitlab.com/asiriyuq/eyes_on_e621
```

### Usage

#### Starting up

```bash
(eyes) $ python eyes_on_e621/bot.py
```

On the first run, a setup script will guide you through the final steps.

Before you run it again to complete the setup, review the options in config.py, particularly the timezone and ownership information.

## Credits

* Almost all of the code that deals with interfacing with the e621 API is heavily modified from [py621 by Hunter-The-Furry](https://github.com/Hunter-The-Furry/py621)
* Consulting and advice from Alexi Wolfe
* Awesome icon artwork provided by [Precious Rat](https://twitter.com/precious_rat)
