import config
import db
import e621
import keys

import time
from datetime import datetime, timedelta
import pytz
import sys
import logging
from telegram import ParseMode, error

# Enables logging
logger = logging.getLogger(__name__)

def botLoop(updater):
    """
    Defines Eyes on e621 bot behavior
    """
    # Connect to the database
    db_conn = db.Connection(keys.database_key)

    # Loads the list of stopped users into memory to greatly reduce database queries
    global stoppedUsers
    stoppedUsers = db_conn.getStoppedUsers()

    # Checks if bot is suspended and notifies admins that the bot is online
    messageContent = "Bot is online and is "
    if db_conn.isSuspended():
        messageContent += "suspended"
    else:
        messageContent += "not suspended"
    for chatID in db_conn.getAllAdmins():
        updater.bot.send_message(chatID, messageContent)

    # Sets the cutoffPost to the newest post on e621 if default value is found in database
    if db_conn.getCutoffPost() == 0:
        cutoffPost = e621.getPosts([], 1, 1)[0].id
        db_conn.updateCutoffPost(cutoffPost)

    # Gets initial cutoffs before entering the loop
    cutoffTime = db_conn.getCutoffTime()
    cutoffTime = datetime.strptime(cutoffTime, "%Y-%m-%d %H:%M:%S.%f%z")
    cutoffPost = db_conn.getCutoffPost()

    # Set needed variables before entering loop
    notifyAdminsIfPostFetchErrorFlag = True

    while True:
        # Checks once every configured amount of time, but intelligently
        now = pytz.timezone(config.pytz_time_zone).localize(datetime.now())
        secondsSinceCutoffTime = (now - cutoffTime).total_seconds()
        if secondsSinceCutoffTime < config.cycle_wait_time:
            # Waits
            logger.info("waiting " + str(config.cycle_wait_time - secondsSinceCutoffTime) + " seconds...")
            try:
                time.sleep(config.cycle_wait_time - secondsSinceCutoffTime)
            except ValueError:
                # handles extremely rare cases where a negative value is computed
                if config.cycle_wait_time - secondsSinceCutoffTime < -1800:
                    logger.error("The current time is significantly behind the most recent time in the database! Did you migrate to a new time zone?")
                    return
                else:
                    pass
            except KeyboardInterrupt:
                logger.info("Keyboard interrupt received! Breaking out of botLoop()...")
                return
        else:
            # Since it has been more than the configured wait time since the last check, runs right away
            pass

        # Checks if bot is suspended
        try:
            if db_conn.isSuspended():
                logger.info("Bot is suspended!")
                while True:
                    time.sleep(config.suspend_check_time)
                    if db_conn.isSuspended():
                        # Bot is still suspended, so wait and check again later
                        pass
                    else:
                        # Resets this flag so admins are notified of errors
                        notifyAdminsIfPostFetchErrorFlag = True

                        # Bot was unsuspended, so breaks out of waiting pattern
                        logger.info("Bot is unsuspended! Resuming...")
                        break
            else:
                pass
        except KeyboardInterrupt:
                logger.info("Keyboard interrupt received! Breaking out of botLoop()...")
                return

        # Keep track of how long it takes to run through the loop for reporting purposes
        loopStartTime = time.time()

        # Ensures that the cutoffTime for next check is as close to the request time as possible
        cutoffTime = pytz.timezone(config.pytz_time_zone).localize(datetime.now())
        db_conn.updateCutoffTime(cutoffTime)

        try:
            # Gets the latest posts since the cutoffPost
            postsListRaw = e621.getPosts([], config.posts_per_request, "a" + str(cutoffPost))
        except:
            logger.error("Error when fetching posts: " + str(sys.exc_info()[1]))

            # Notifies admins, but only once
            if notifyAdminsIfPostFetchErrorFlag:
                for chatID in db_conn.getAllAdmins():
                    updater.bot.send_message(chatID,"Bot failed to fetch posts: " + str(sys.exc_info()[1]))
            notifyAdminsIfPostFetchErrorFlag = False

            # Provides an empty list of returned posts
            postsListRaw = []
        else:
            # Since fetching the posts succeeded, toggles the flag to notify admins next time problems are encountered
            notifyAdminsIfPostFetchErrorFlag = True

        # Reverses the list so that the posts are processed in the correct chronological order
        postsListRaw.reverse()

        # Adds delay to filter out spam and (hopefully) to give enough time for e621 members to tag something that was blatantly missed
        postsList = []
        now = pytz.timezone(config.pytz_time_zone).localize(datetime.now())
        for post in postsListRaw:
            if (now - post.created_at).total_seconds() > config.candidate_post_delay:
                postsList.append(post)
        logger.info("After applying delay, " + str(len(postsList)) + " posts considered")

        # Skips entire cycle if no posts considered
        if len(postsList):
            # Keeps track of the most recently seen post, but keeps the former post in memory in case of an error
            formerCutoffPost = cutoffPost
            for post in postsList:
                if post.id > cutoffPost:
                    cutoffPost = post.id
            if cutoffPost != formerCutoffPost:
                logger.info("New cutoff post: " + str(cutoffPost))
                db_conn.updateCutoffPost(cutoffPost)

            # Checks posts per user
            for chatID in db_conn.getActiveUsers():
                stopUserFlag = False

                # Gets needed information about the user
                watchedTags, incidentalTags, blacklistedTags = db_conn.getUserTags(chatID)
                watchedPools = db_conn.getUserPools(chatID)
                maxRating = db_conn.getUserMaxRating(chatID)

                for post in postsList:
                    # Only considers posts that match the user's tag criteria, maximum rating preference, and that are not deleted
                    if ( ( any(tag in post.tags for tag in watchedTags) and not any(tag in post.tags for tag in blacklistedTags) ) or any(poolID in post.pools for poolID in watchedPools) ) and post.rating <= maxRating and not post.deleted:

                        # Finds tag hits
                        hitTags = [tag for tag in post.tags if (tag in watchedTags or tag in incidentalTags) and not tag in post.artists]

                        # Generates metadata message content
                        messageContent = post.prettyMessage()
                        for tag in hitTags:
                            messageContent += "\n#" + str(tag)

                        # Sends metadata message
                        try:
                            updater.bot.send_message(chatID, messageContent, parse_mode = ParseMode.HTML, disable_web_page_preview = False)
                            logger.debug("Sent post " + str(post.id) + " to user " + str(chatID) + ", matched tags " + str(hitTags))
                        except error.BadRequest as updateError:
                            if str(updateError) == "Chat not found":
                                # Assumes that user cannot be reached or does not want to be reached and stops them. Flags user for deletion
                                logger.debug("Failed to send message to user " + str(chatID) + "Flagging user to be stopped at end of this cycle")
                                stopUserFlag = True
                            else:
                                # Just in case something else can cause this error, log it and don't assume the user should be stopped
                                logger.error("Error when trying to send post to user: " + str(sys.exc_info()[0]))
                        except error.Unauthorized as updateError:
                            if str(updateError) == "Forbidden: bot was blocked by the user" or str(updateError) == "Forbidden: user is deactivated":
                                # Flags user for stopping
                                logger.debug("Failed to send message to user " + str(chatID) + "Flagging user to be stopped at end of this cycle")
                                stopUserFlag = True
                            else:
                                # Just in case something else can cause this error, log it and don't assume the user should be stopped
                                logger.error("Error when trying to send post to user: " + str(sys.exc_info()[0]))
                        except error.NetworkError:
                            # It is preferrable to have repeats over missing content, so resets the post tracking and tries again later
                            logging.error("[1/2] Error when trying to send post to user: " + str(sys.exc_info()[0]))
                            logging.error("[2/2] Resetting post tracking and trying again later")
                            db_conn.updateCutoffPost(formerCutoffPost)
                            time.sleep(config.cycle_wait_time)
                    else:
                        pass

                if stopUserFlag:
                    # Since user couldn't be reached, stops them
                    logger.debug("Stopping " + str(chatID))
                    db_conn.stopUser(chatID)
                    stoppedUsers.append(chatID)

            logger.info("This cycle took " + str((time.time() - loopStartTime)) + " seconds")
        else:
            logger.info("Skipping cycle because no posts were considered")
            
