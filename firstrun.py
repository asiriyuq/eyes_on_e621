import crypto
import db

import textwrap
import getpass

def firstRun():
    """
    Sets up encrypted key store and database at first run
    """
    # Enables text wrapping
    wrapper = textwrap.TextWrapper(width=70)

    # Greets the user and explains what is happening
    messageGreeting = """It appears that this is the first time you are running the bot. This
setup script will help you get started. You will need to provide some keys and passwords
(refer to the README in case you aren't sure how to get the required information)."""
    messageGreeting = wrapper.wrap(messageGreeting)
    for line in messageGreeting:
        print(line)

    # Gets secrets from the user
    chatID = input("\n> Telegram chatID: ")
    api_key_telegram = input("> Telegram bot API key: ")
    username_e621 = input("> e621.net username: ")
    api_key_e621 = input("> e621.net API key: ")
    password = getpass.getpass("> Password: ")

    # Summarizes the info
    print("\nSummary:")
    print("-- Telegram chatID: " + chatID)
    print("-- Telegram API key: " + api_key_telegram)
    print("-- e621.net username: " + username_e621)
    print("-- e621.net API key: " + api_key_e621)
    print("-- Password: " + len(password) * "*")

    # Asks the user to verify the information
    ok = input("\n> Is this information correct? [Y/n]: ")
    if not ok in ["", "y", "Y", "Yes", "yes", "YES"]:
        print("\nAborted setup!")
        quit()
    else:
        pass

    # Sets up the database
    print("\nGenerating database key...", end = "")
    database_key = crypto.generateDatabaseKey()
    db_conn = db.Connection(database_key)
    db_conn.setUpFreshDatabase()
    db_conn.createNewUser(chatID)
    db_conn.promoteUser(chatID)
    print("OK")

    # Generates keys.py
    print("Generating keys.py...", end = "")
    api_key_telegram = crypto.encrypt(password, api_key_telegram)
    api_key_e621 = crypto.encrypt(password, api_key_e621)
    database_key = crypto.encrypt(password, database_key)

    fileContent = '''# This file was automatically generated from setup.py
import crypto
import logging

# Enables logging
logger = logging.getLogger(__name__)

firstRunFlag = False

def decryptKeys(password):
    """
    Decrypts the encrypted variables stored in this file
    """
    global api_key_telegram, username_e621, api_key_e621, database_key

    api_key_telegram = ''' + str(api_key_telegram) + '''
    username_e621 = "''' + str(username_e621) + '''"
    api_key_e621 = ''' + str(api_key_e621) + '''
    database_key = ''' + str(database_key) + '''

    try:
        api_key_telegram = crypto.decrypt(password, api_key_telegram)
        api_key_e621 = crypto.decrypt(password, api_key_e621)
        database_key = crypto.decrypt(password, database_key)
    except:
        logger.error("Incorrect password")
        quit()
'''

    keysFile = open("keys.py", "w")
    keysFile.write(fileContent)
    print("OK\n")

    # Closes out the setup with a message
    closeoutMessage = """The automated setup is complete! You should save your password
somewhere safe like a password manager. Before running the bot again, be sure to check
config.py and manually change those settings if you need to."""
    closeoutMessage = wrapper.wrap(closeoutMessage)
    for line in closeoutMessage:
        print(line)
    print("")

    quit()
