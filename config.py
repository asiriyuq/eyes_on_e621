import logging

# Logging module configuration
log_level = logging.INFO # (DEBUG/INFO/WARNING/ERROR/CRITICAL)
log_format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"

# Time zone
pytz_time_zone = "America/New_York" # Must be in pytz.all_timezones

# Performance settings
cycle_wait_time = 180 # Minimum seconds between requests for more posts
posts_per_request = 100 # Maximum number of posts returned per request (hard limit of 320 imposed by e621)
candidate_post_delay = 3600 # Minimum seconds since post created until it is considered
suspend_check_time = 10 # Seconds between attempts to unsuspend the bot when suspened

# Sqlcipher module compatibility
sqlcipher_compatibility = 0 # Change to major version (1/2/3/4) if using new major sqlcipher version on a legacy database

# e621 API configuration
e621_headers = {"User-Agent": "Eyes_on_e621/2.2 (by Asiriyuq on Telegram)"} # e621 will blacklist you if not in correct format

# Ownership information
admin_contact = "@Asiriyuq" # Telegram handle
