import config

from sqlcipher3 import dbapi2 as sqlcipher3
import logging

# Enables logging
logger = logging.getLogger(__name__)

def unTupledList(dirtyList):
    """
    Transforms sqlite's list of tuples to a more friendly format
    """
    cleanList = []
    for item in dirtyList:
        cleanList.append(item[0])
    return cleanList

class Connection():
    def __init__(self, password):
        """
        Connects to the encrypted database
        """
        self.password = password
        self.conn = sqlcipher3.connect("eyes.db", timeout = 20)
        self.conn.execute("PRAGMA key = '" + password + "'")
        
        # Sets compatibility mode if needed
        if config.sqlcipher_compatibility:
            self.conn.execute("PRAGMA cipher_compatibility = " + str(config.sqlcipher_compatibility))
        
        logger.debug("Connection to the database established")

    def exportToPlaintext(self):
        """
        Dumps the database to a plaintext file. Dangerous for security! This is provided only as a developer's convinience
        """
        self.conn.execute("ATTACH DATABASE 'plaintext.db' AS plaintext KEY ''")
        self.conn.execute("SELECT sqlcipher_export('plaintext')")
        self.conn.execute("DETACH DATABASE plaintext")

    def importFromPlaintext(self):
        """
        Imports the data from a plaintext database. Assumes that eyes.db is empty and that the plaintext database is set up correctly
        """
        temp_conn = sqlcipher3.connect("plaintext.db")
        temp_conn.execute("ATTACH DATABASE 'eyes.db' AS eyes KEY ?", (self.password,))
        temp_conn.execute("SELECT sqlcipher_export('eyes')")
        temp_conn.execute("DETACH DATABASE eyes")

    def migrateToNewVersion(self):
        """
        Performs an in-place database migration to the newest sqlcipher version available. This is provided only as a developer's convinience
        """
        self.conn.execute("PRAGMA cipher_migrate")

    def setUpFreshDatabase(self):
        """
        Sets up the database schema. Should only run on first startup!
        """
        # Creates the users table. It contains user settings (one row per user)
        self.conn.execute("""CREATE TABLE users(
            chatID INTEGER,
            isStopped INTEGER,
            isAdmin INTEGER,
            maxRating INTEGER
            )""")

        # Creates the metadata table. It will help the bot keep track of what it is doing
        self.conn.execute("""CREATE TABLE meta(
            cutoffPost INTEGER,
            cutoffTime TEXT,
            suspended INTEGER
            )""")
        # Inserts some default values to avoid errors when the bot accesses this table for the first time
        self.conn.execute("INSERT INTO meta (cutoffPost, cutoffTime) VALUES (?, ?)", (0, "2020-01-01 00:00:00.000000-04:00"))
        
        # Creates the watched tags table
        self.conn.execute("""CREATE TABLE watchedTags(
            chatID INTEGER,
            tag TEXT
            )""")

        # Creates the incidental tags table
        self.conn.execute("""CREATE TABLE incidentalTags(
            chatID INTEGER,
            tag TEXT
            )""")
        
        # Creates the blacklisted tags table
        self.conn.execute("""CREATE TABLE blacklistedTags(
            chatID INTEGER,
            tag TEXT
            )""")
        
        # Creates the watched pools table
        self.conn.execute("""CREATE TABLE watchedpools(
            chatID INTEGER,
            poolID INTEGER
            )""")
        
        # Writes the new tables to the database
        self.conn.commit()
        logger.debug("Wrote to eyes.db: Set up fresh database")

    def isAdmin(self, chatID):
        """
        Checks to see if a particular user is an admin
        """
        cursor = self.conn.execute("SELECT isAdmin FROM users WHERE chatID = ?", (chatID,))
        isAdminFlag = cursor.fetchone()
        if isAdminFlag == None:
            return False
        if isAdminFlag[0]:
            return True
        else:
            return False

    def getAllAdmins(self):
        """
        Gets a list of the chatIDs of all admins
        """
        cursor = self.conn.execute("SELECT chatID FROM users WHERE isAdmin = 1")
        return unTupledList(cursor.fetchall())

    def getAllUsers(self):
        """
        Gets a list of all chatIDs
        """
        cursor = self.conn.execute("SELECT chatID FROM users")
        return unTupledList(cursor.fetchall())

    def getActiveUsers(self):
        """
        Gets a list of all users who are not stopped and who are watching at least one tag
        """
        cursor = self.conn.execute("SELECT chatID FROM users WHERE isStopped = 0 AND chatID IN (SELECT chatID FROM watchedTags)")
        return unTupledList(cursor.fetchall())
    
    def getStoppedUsers(self):
        """
        Gets a list of all stopped users
        """
        cursor = self.conn.execute("SELECT chatID FROM users WHERE isStopped = 1")
        return unTupledList(cursor.fetchall())

    def getDormantUsers(self):
        """
        Gets a list of users who are not stopped and who are not following any tags
        """
        cursor = self.conn.execute("SELECT chatID FROM users WHERE isStopped = 0 AND chatID NOT IN (SELECT chatID FROM watchedTags)")
        return unTupledList(cursor.fetchall())

    def updateCutoffTime(self, value):
        """
        Updates cutoffTime
        """
        self.conn.execute("UPDATE meta SET cutoffTime = ?", (value,))
        self.conn.commit()
        logger.debug("Wrote to eyes.db: cutoffTime")

    def updateCutoffPost(self, value):
        """
        Updates cutoffPost
        """
        self.conn.execute("UPDATE meta SET cutoffPost = ?", (value,))
        self.conn.commit()
        logger.debug("Wrote to eyes.db: cutoffPost")

    def getCutoffTime(self):
        """
        Gets the cutoffTime
        """
        cursor = self.conn.execute("SELECT cutoffTime from meta")
        result = cursor.fetchall()
        return result[0][0]

    def getCutoffPost(self):
        """
        Gets the cutoffPost
        """
        cursor = self.conn.execute("SELECT cutoffPost from meta")
        result = cursor.fetchall()
        return result[0][0]

    def isSuspended(self):
        """
        Checks to see if the bot should be suspended or not
        """
        cursor = self.conn.execute("SELECT suspended from meta")
        result = cursor.fetchall()
        if result[0][0] == 0:
            return False
        else:
            return True

    def suspend(self):
        """
        Updates the suspended flag to true
        """
        self.conn.execute("UPDATE meta SET suspended = 1")
        self.conn.commit()
        logger.debug("Wrote to eyes.db: suspend")

    def unsuspend(self):
        """
        Updates the suspended flag to false
        """
        self.conn.execute("UPDATE meta SET suspended = 0")
        self.conn.commit()
        logger.debug("Wrote to eyes.db: suspend")

    def createNewUser(self, chatID):
        """
        Creates or un-stops user if necessary and reports what changed
        """
        cursor = self.conn.execute("SELECT isStopped FROM users WHERE chatID = ?", (chatID,))
        result = cursor.fetchall()

        if result == []:
            # Creates new entry for brand new user
            self.conn.execute("INSERT INTO users (chatID, isStopped, isAdmin, maxRating) VALUES (?, ?, ?, ?)", (chatID, 0, 0, 0))
            self.conn.commit()
            logger.debug("Wrote to eyes.db: Created new user")
            return "▶️ A new user has started the bot"
        elif result == [(1,)]:
            # Un-stops previously stopped user
            self.conn.execute("UPDATE users SET isStopped = 0 WHERE chatID = ?", (chatID,))
            self.conn.commit()
            logger.debug("Wrote to eyes.db: Updated existing user")
            return "▶️ A previously stopped user has started the bot"
        else:
            # User already exists and is not stopped, so do nothing
            return None

    def promoteUser(self, chatID):
        """
        Makes a user an administrator
        """
        self.conn.execute("UPDATE users SET isAdmin = 1 WHERE chatID = ?", (chatID,))
        self.conn.commit()
        logger.debug("Wrote to eyes.db: Made user administrator")

    def demoteUser(self, chatID):
        """
        Makes an administrator a user
        """
        self.conn.execute("UPDATE users SET isAdmin = 0 WHERE chatID = ?", (chatID,))
        self.conn.commit()
        logger.debug("Wrote to eyes.db: Made administrator user")

    def stopUser(self, chatID):
        """
        Deletes and stops a user
        """
        # Deletes all tags and pools
        self.conn.execute("DELETE FROM watchedTags WHERE chatID = ?", (chatID,))
        self.conn.execute("DELETE FROM blacklistedTags WHERE chatID = ?", (chatID,))
        self.conn.execute("DELETE FROM incidentalTags WHERE chatID = ?", (chatID,))
        self.conn.execute("DELETE FROM watchedPools WHERE chatID = ?", (chatID,))
        # Resets settings and stops user
        self.conn.execute("UPDATE users SET (isStopped, maxRating) = (?, ?) WHERE chatID = ?", (1, 0, chatID))
        self.conn.commit()

    def getUserMaxRating(self, chatID):
        """
        Gets a user's maximum rating setting
        """
        cursor = self.conn.execute("SELECT maxRating FROM users WHERE chatID = ?", (chatID,))
        result = cursor.fetchall()
        return result[0][0]
        

    def updateUserMaxRating(self, chatID, maxRating):
        """
        Updates the user's maximum rating setting
        """
        self.conn.execute("UPDATE users SET maxRating = ? WHERE chatID = ?", (maxRating, chatID))
        self.conn.commit()
        logger.debug("Wrote to eyes.db: New maxrating")

    def getUserTags(self, chatID):
        """
        Gets the list of watched tags and the list of blacklisted tags for a user
        """
        # Gets all watched tags
        watchedTagsCursor = self.conn.execute("SELECT tag FROM watchedTags WHERE chatID = ?", (chatID,))
        watchedTags = watchedTagsCursor.fetchall()

        # Gets all incidental tags
        incidentalTagsCursor = self.conn.execute("SELECT tag FROM incidentalTags WHERE chatID = ?", (chatID,))
        incidentalTags = incidentalTagsCursor.fetchall()

        # Gets all blacklisted tags
        blacklistedTagsCursor = self.conn.execute("SELECT tag FROM blacklistedTags WHERE chatID = ?", (chatID,))
        blacklistedTags = blacklistedTagsCursor.fetchall()

        return unTupledList(watchedTags), unTupledList(incidentalTags), unTupledList(blacklistedTags)

    def getUserPools(self, chatID):
        """
        Gets the list of watched pools for a user
        """
        cursor = self.conn.execute("SELECT poolID FROM watchedPools WHERE chatID = ?", (chatID,))
        return unTupledList(cursor.fetchall())

    def addWatchedTags(self, chatID, tags):
        """
        Adds a list of tags to a user's list of watched tags
        """
        for tag in tags:
            self.conn.execute("INSERT INTO watchedTags (chatID, tag) VALUES (?, ?)", (chatID, tag))
        self.conn.commit()
        logger.debug("Wrote to eyes.db: added to watchedTags")

    def addIncidentalTags(self, chatID, tags):
        """
        Adds a list of tags to a user's list of incidental tags
        """
        for tag in tags:
            self.conn.execute("INSERT INTO incidentalTags (chatID, tag) VALUES (?, ?)", (chatID, tag))
        self.conn.commit()
        logger.debug("Wrote to eyes.db: added to incidentalTags")

    def addBlacklistedTags(self, chatID, tags):
        """
        Adds a list of tags to a user's list of blacklisted tags
        """
        for tag in tags:
            self.conn.execute("INSERT INTO blacklistedTags (chatID, tag) VALUES (?, ?)", (chatID, tag))
        self.conn.commit()
        logger.debug("Wrote to eyes.db: added to blacklistedTags")

    def addWatchedPools(self, chatID, pools):
        """
        Adds a list of pools to a user's list of watched pools
        """
        for poolID in pools:
            self.conn.execute("INSERT INTO watchedPools (chatID, poolID) VALUES (?, ?)", (chatID, poolID))
        self.conn.commit()
        logger.debug("Wrote to eyes.db: added to watchedPools")

    def removeWatchedTags(self, chatID, tags):
        """
        Removes a list of tags from a user's list of watched tags
        """
        for tag in tags:
            self.conn.execute("DELETE FROM watchedTags WHERE (chatID, tag) = (?, ?)", (chatID, tag))
        self.conn.commit()
        logger.debug("Wrote to eyes.db: removed from watchedTags")

    def removeIncidentalTags(self, chatID, tags):
        """
        Removes a list of tags from a user's list of incidental tags
        """
        for tag in tags:
            self.conn.execute("DELETE FROM incidentalTags WHERE (chatID, tag) = (?, ?)", (chatID, tag))
        self.conn.commit()
        logger.debug("Wrote to eyes.db: removed from incidentalTags")

    def removeBlacklistedTags(self, chatID, tags):
        """
        Removes a list of tags from a user's list of blacklisted tags
        """
        for tag in tags:
            self.conn.execute("DELETE FROM blacklistedTags WHERE (chatID, tag) = (?, ?)", (chatID, tag))
        self.conn.commit()
        logger.debug("Wrote to eyes.db: removed from blacklistedTags")

    def removeWatchedPools(self, chatID, pools):
        """
        Removes a list of pools from a user's list of watched pools
        """
        for poolID in pools:
            self.conn.execute("DELETE FROM watchedPools WHERE (chatID, poolID) = (?, ?)", (chatID, poolID))
        self.conn.commit()
        logger.debug("Wrote to eyes.db: removed from watchedPools")
