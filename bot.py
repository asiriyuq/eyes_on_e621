import config
import keys
import eyes
import handlers
import firstrun

import logging
import getpass
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import pytz

# Enables logging
logging.basicConfig(format=config.log_format, level=config.log_level)
logger = logging.getLogger(__name__)

def checkConfig():
    """
    Sanity checks config.py to get ahead of potentially cryptic errors later
    """
    # Checks logging config
    if not config.log_level in [logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.CRITICAL]:
        raise EyesConfigError("log_level must be a valid python logging level.")
    if not type(config.log_format) == str:
        raise EyesConfigError("log_format must be a string.")

    # Checks pytz config
    if not config.pytz_time_zone in pytz.all_timezones:
        raise EyesConfigError("pytz_time_zone is not a valid pytz time zone.")

    # Checks performance settings
    for setting in [config.cycle_wait_time, config.posts_per_request, config.candidate_post_delay, config.suspend_check_time]:
        if not type(setting) == int:
            raise EyesConfigError("Performance settings must be integers.")
        if setting < 0:
            raise EyesConfigError("Performance settings must be greater than zero.")

    # Warns user if performance settings are suboptimal
    if config.cycle_wait_time < 60:
        logger.warning("cycle_wait_time is low, you may be generating more e621 API calls than you need to.")
    if config.posts_per_request < 10:
        logger.warning("posts_per_request is low, you may not be getting enough data per e621 API call.")
    if config.posts_per_request > 320:
        logger.warning("posts_per_request is greater than the cap imposed by e621. Only fetching 320 posts per e621 API call.")
    if config.candidate_post_delay < 600:
        logger.warning("candidate_post_delay is low, you may have problems with spam or chronically mistagged posts.")
    if config.candidate_post_delay > 14400:
        logger.warning("candidate_post_delay is high, you are running significantly behind the front page of e621.")
    if config.suspend_check_time > 30:
        logger.warning("suspend_check_time is high, there may be a significant delay after issuing the unsuspend command")

    # Checks sqlcipher module compatibility config
    if not type(config.sqlcipher_compatibility) == int:
        raise EyesConfigError("sqlcipher_compatibility must be an integer.")
    if config.sqlcipher_compatibility < 0:
        raise EyesConfigError("sqlcipher_compatibility must not be negative.")
    if config.sqlcipher_compatibility > 4:
        logger.warning("Unrecognized sqlcipher version. Running anyway.")

    # Checks e621 API config
    try:
        if type(config.e621_headers["User-Agent"]) != str:
            raise EyesConfigError("e621 API user agent must be a string.")
    except KeyError:
        raise EyesConfigError("e621 API user agent must be specified.")

    # Checks ownership config
    if type(config.admin_contact) != str:
        raise EyesConfigError("admin_contact must be a string.")
    if config.admin_contact[0] != "@":
        raise EyesConfigError("admin_contact must be a Telegram handle (starts with @).")

def main():
    """
    Starts the bot
    """
    # Determines if first run
    if keys.firstRunFlag:
        # firstrun.py will quit() after it is done
        firstrun.firstRun()

    # Checks config.py
    checkConfig()

    # Decrypts keys.py using user-supplied password
    password = getpass.getpass()
    keys.decryptKeys(password)

    # Creates the Updater and passes it the token
    updater = Updater(keys.api_key_telegram, use_context=True)
    # Gets the dispatcher to register handlers
    dp = updater.dispatcher

    # On user commands...
    dp.add_handler(CommandHandler("start", lambda update, context: handlers.start(update, context, updater)))
    dp.add_handler(CommandHandler("stop", handlers.stop))
    dp.add_handler(CommandHandler("stop_confirm", lambda update, context: handlers.stop_confirm(update, context, updater)))

    dp.add_handler(CommandHandler("help", handlers.help))
    dp.add_handler(CommandHandler("faq", handlers.faq))

    dp.add_handler(CommandHandler("showtags", handlers.showtags))
    dp.add_handler(CommandHandler("listtags", handlers.showtags)) # alias
    dp.add_handler(CommandHandler("list", handlers.showtags)) # alias

    dp.add_handler(CommandHandler("addtags", lambda update, context: handlers.addtagstolist(update, context, "watchlist")))
    dp.add_handler(CommandHandler("addtag", lambda update, context: handlers.addtagstolist(update, context, "watchlist"))) # alias
    dp.add_handler(CommandHandler("addbl", lambda update, context: handlers.addtagstolist(update, context, "blacklist")))
    dp.add_handler(CommandHandler("addincs", lambda update, context: handlers.addtagstolist(update, context, "incidentals")))
    dp.add_handler(CommandHandler("addinc", lambda update, context: handlers.addtagstolist(update, context, "incidentals"))) # alias
    dp.add_handler(CommandHandler("addinctags", lambda update, context: handlers.addtagstolist(update, context, "incidentals"))) # alias

    dp.add_handler(CommandHandler("remtags", lambda update, context: handlers.removetagsfromlist(update, context, "watchlist")))
    dp.add_handler(CommandHandler("remtag", lambda update, context: handlers.removetagsfromlist(update, context, "watchlist"))) # alias
    dp.add_handler(CommandHandler("removetags", lambda update, context: handlers.removetagsfromlist(update, context, "watchlist"))) # alias
    dp.add_handler(CommandHandler("removetag", lambda update, context: handlers.removetagsfromlist(update, context, "watchlist"))) # alias
    dp.add_handler(CommandHandler("remincs", lambda update, context: handlers.removetagsfromlist(update, context, "incidentals")))
    dp.add_handler(CommandHandler("reminc", lambda update, context: handlers.removetagsfromlist(update, context, "incidentals"))) # alias
    dp.add_handler(CommandHandler("removeincs", lambda update, context: handlers.removetagsfromlist(update, context, "incidentals"))) # alias
    dp.add_handler(CommandHandler("rembl", lambda update, context: handlers.removetagsfromlist(update, context, "blacklist")))
    dp.add_handler(CommandHandler("removebl", lambda update, context: handlers.removetagsfromlist(update, context, "blacklist"))) # alias

    dp.add_handler(CommandHandler("showpools", handlers.showpools))
    dp.add_handler(CommandHandler("listpools", handlers.showpools)) # alias

    dp.add_handler(CommandHandler("addpools", handlers.addpools))

    dp.add_handler(CommandHandler("rempools", handlers.rempools))
    dp.add_handler(CommandHandler("removepools", handlers.rempools)) # alias

    dp.add_handler(CommandHandler("maxrating", handlers.maxrating))
    dp.add_handler(CommandHandler("maxrating_safe", lambda update, context: handlers.setmaxrating(update, context, 0)))
    dp.add_handler(CommandHandler("maxrating_questionable", lambda update, context: handlers.setmaxrating(update, context, 1)))
    dp.add_handler(CommandHandler("maxrating_explicit", lambda update, context: handlers.setmaxrating(update, context, 2)))

    dp.add_handler(CommandHandler("testcon", handlers.testcon))

    dp.add_handler(CommandHandler("cancel", handlers.cancel))
    
    # On administrator commands...
    dp.add_handler(CommandHandler("announce", lambda update, context: handlers.announce(update, context, updater)))
    dp.add_handler(CommandHandler("userstats", handlers.userstats))
    dp.add_handler(CommandHandler("promote", handlers.promote))
    dp.add_handler(CommandHandler("demote", handlers.demote))
    dp.add_handler(CommandHandler("showadmins", handlers.showadmins))
    dp.add_handler(CommandHandler("suspend", handlers.suspend))
    dp.add_handler(CommandHandler("unsuspend", handlers.unsuspend))

    # On noncommands i.e. regular messages...
    dp.add_handler(MessageHandler(Filters.text & ~Filters.command, handlers.noncommand))

    # Starts the Telegram bot
    updater.start_polling()

    # Starts the Eyes on e621 bot
    eyes.botLoop(updater)

    # Stops the bot in case botLoop() breaks
    updater.stop()


if __name__ == '__main__':
    main()
