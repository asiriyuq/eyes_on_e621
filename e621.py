import keys
import config

from tldextract import extract as tldEx
import requests
import json
import time
from datetime import datetime, timezone
import logging

# Enables logging
logger = logging.getLogger(__name__)

# Exeptions for commonly-cited sources (domain.suffix: correction)
sourceExceptions = {
                "t.me": "telegram",
                "twimg.com": "twitter",
                "sofurryfiles.com": "sofurry",
                "pximg.net": "pixiv",
                "derpicdn.net": "derpibooru",
                "youtu.be": "youtube",
                "ungrounded.net": "newgrounds",
                "discordapp.com": "discord",
                "metapix.net": "inkbunny",
                "sinaimg.cn": "weibo",
                "desu-usergeneratedcontent.xyz": "desuarchive",
                "dropboxusercontent.com": "dropbox"}

# Ignored tags lists for pretty printing (not comprehensive, must account for common tagging errors)
ignoredArtists = ["conditional_dnp",  "unknown_artist", "unknown_artist_signature", "unknown_colorist",
                "unknown_artist_(artist)", "unknown_(artist)", "unknown_artists", "unknown_editor", "unknown_person"]
ignoredCopyright = ["unknown_copyright", "unknown_(copyright)"]

def handleCodes(StatusCode):
    """
    Handles status codes
    """
    if StatusCode == 200:
        return
    else:
        Codes = {
            "401": "API Key Error",
            "403": "Forbidden; Access denied",
            "404": "Not found",
            "412": "Precondition failed",
            "420": "Invalid Record; Record could not be saved",
            "421": "User Throttled; User is throttled, try again later",
            "422": "Locked; The resource is locked and cannot be modified",
            "423": "Already Exists; Resource already exists",
            "424": "Invalid Parameters; The given parameters were invalid",
            "500": "Internal Server Error; Some unknown error occurred on the server",
            "502": "Bad Gateway; A gateway server received an invalid response from the e621 servers",
            "503": "Service Unavailable; Server cannot currently handle the request or you have exceeded the request rate limit. Try again later or decrease your rate of requests.",
            "520": "Unknown Error; Unexpected server response which violates protocol",
            "522": "Origin Connection Time-out; CloudFlare's attempt to connect to the e621 servers timed out",
            "524": "Origin Connection Time-out; A connection was established between CloudFlare and the e621 servers, but it timed out before an HTTP response was received",
            "525": "SSL Handshake Failed; The SSL handshake between CloudFlare and the e621 servers failed"}
        try:
            raise ConnectionRefusedError(
                "Server connection refused! HTTP Status code: " + str(StatusCode) + " " + Codes[str(StatusCode)])
        except KeyError:  # This shouldn't happen
            raise ConnectionRefusedError(
                "Server connection refused! HTTP Status code: " + str(StatusCode) + " Unknown status code")

class Post:
    def __init__(self):
        pass

    def prettyMessage(self):
        # Rating indicator doubles as sample URL for Telegram preview
        if self.rating == 2:
            string = '<a href="' + str(self.sample_url) + '">🔴</a> '
        elif self.rating == 1:
            string = '<a href="' + str(self.sample_url) + '">🟡</a> '
        else:
            string = '<a href="' + str(self.sample_url) + '">🟢</a> '

        # Page URL
        string += '<a href="https://e621.net/posts/' + str(self.id) + '">[Page]</a> '

        # File URL
        string += '<a href="' + str(self.file_url) + '">[File]</a> '

        # File metadata
        string += '(' + str(round(self.file_size/1048576, 1)) + "MB " + str(self.file_ext) + ')'

        # Artists, if any provided
        if self.artists:
            string += '\nArtists:'
            for artist in self.artists:
                string += ' <a href="https://e621.net/posts?tags=' + str(artist) + '">[' + str(artist) + ']</a>'

        # Copyright, if any provided
        if self.copyright:
            string += '\nCopyright:'
            for copyright in self.copyright:
                string += ' <a href="https://e621.net/posts?tags=' + str(copyright) + '">[' + str(copyright) + ']</a>'

        # Source URLs, if any provided
        if self.sources:
            string += '\nSources:'
            for source in self.sources:
                # Checks exceptions for problematic commonly-cited source domains
                if tldEx(source).domain + "." + tldEx(source).suffix in sourceExceptions:
                    string += ' <a href="' + str(source) + '">[' + sourceExceptions[tldEx(source).domain + "." + tldEx(source).suffix] + ']</a>'
                # Handles Tor network sources
                elif tldEx(source).suffix == "onion":
                    string += ' <a href="' + str(source) + '">[Tor network]</a>'
                else:
                    string += ' <a href="' + str(source) + '">[' + str(tldEx(source).domain) + ']</a>'

        # Pool IDs, if any provided
        if self.pools:
            string += '\nPools:'
            for poolID in self.pools:
                string += ' <a href="https://e621.net/pools/' + str(poolID) + '">[' + str(poolID) + ']</a>'

        return string

def jsonToPost(List):
    """
    Converts the API's json output into a Post object. The conversion is non-exhaustive and the fidelity is dictated by the bundled bot.
    """
    ThisPost = Post()

    # Stores most of the metadata as-is
    ThisPost.id = List["id"]
    ThisPost.file_ext = List["file"]["ext"]
    ThisPost.file_size = List["file"]["size"]
    ThisPost.file_url = List["file"]["url"]
    ThisPost.sample_url = List["sample"]["url"]
    ThisPost.pools = List["pools"]
    ThisPost.deleted = List["flags"]["deleted"]
    ThisPost.score = List["score"]["total"]

    # Ensures that sources are actually correct (sometimes e621 sends an empty string for some reason)
    ThisPost.sources = [source for source in List["sources"] if source]

    # Stores the creation date as a timezone-aware datetime object
    ThisPost.created_at = datetime.strptime(List["created_at"], "%Y-%m-%dT%H:%M:%S.%f%z")

    # Consolidates tags into a single list
    ThisPost.tags = List["tags"]["general"] + List["tags"]["species"] + List["tags"]["character"] + List["tags"]["artist"] + \
                    List["tags"]["invalid"] + List["tags"]["lore"] + List["tags"]["meta"] + List["tags"]["copyright"] + List["locked_tags"] 
    # Keeps a special section just for printable artists
    ThisPost.artists = [tag for tag in List["tags"]["artist"] if tag not in ignoredArtists]
    # Keeps a special section just for printable copyright
    ThisPost.copyright = [tag for tag in List["tags"]["copyright"] if tag not in ignoredCopyright]

    # Converts the rating into a number on a scale from zero to two
    if List["rating"] == "e":
        ThisPost.rating = 2
    elif List["rating"] == "q":
        ThisPost.rating = 1
    elif List["rating"] == "s":
        ThisPost.rating = 0

    return ThisPost

def getPosts(Tags, Limit, Page):
    """
    Performs a search
    """
    RequestURL = "https://e621.net/posts.json?"

    # Limits the number of posts that the api will return
    RequestURL += "limit="
    RequestURL += str(Limit)

    # Specifies the page to start searching from. Starts at 1.
    RequestURL += "&page="
    RequestURL += str(Page)

    # Handles tag formatting
    RequestURL += "&tags="
    for id, Tag in enumerate(Tags):
        RequestURL += Tag
        if id != (len(Tags) - 1):
            RequestURL += "+"

    # Sends the actual request
    logger.debug(RequestURL)
    eRequest = requests.get(RequestURL, headers=config.e621_headers, auth=(keys.username_e621, keys.api_key_e621))

    # Verifies status code
    handleCodes(eRequest.status_code)

    # Decodes the json into a list
    eJSON = eRequest.json()

    # For every post in the json output, converts to Post object and appends to list
    postsList = []
    for post in eJSON["posts"]:
        postsList.append(jsonToPost(post))
    logger.info("got " + str(len(postsList)) + " posts")

    # Returns list of Post objects
    return postsList

def isTag(Tag):
        """
        Checks if a tag is valid and get alias if it exists
        """
        # Adds in a delay to prevent rate limiting
        time.sleep(1)

        RequestURL = "https://e621.net/tags.json?search[name_matches]=" + Tag

        # Sends the actual request
        logger.debug(RequestURL)
        eRequest = requests.get(RequestURL, headers=config.e621_headers, auth=(keys.username_e621, keys.api_key_e621))

        # Verifies status code
        handleCodes(eRequest.status_code)

        # Decodes the json into a list
        eJSON = eRequest.json()

        try:
            # This throws an error when either the tag does not exist or the tag is aliased
            if eJSON[0]["name"] == Tag:
                # Since the response's name is the same as the queried tag, confirms that the tag exists
                return (True, None)
            else: # This shouldn't happen
                return (False, None)
        # Makes another request to check whether the tag does not exist or if the tag is alised
        except:
            # Adds in a delay to prevent rate limiting
            time.sleep(1)

            RequestURL = "https://e621.net/tag_aliases.json?search[name_matches]=" + Tag

            # Sends the actual request
            logger.debug(RequestURL)
            eRequest = requests.get(RequestURL, headers=config.e621_headers, auth=(keys.username_e621, keys.api_key_e621))

            # Verify status code
            handleCodes(eRequest.status_code)

            # Decodes the json into a list
            eJSON = eRequest.json()

            try:
                # This throws an error when the tag actually does not exist
                if eJSON[0]["antecedent_name"] == Tag:
                    # Returns the alias
                    return (True, eJSON[0]["consequent_name"])
                else: # This shouldn't happen
                    return (False, None)
            except:
                # Confirms that this tag really does not exist
                return (False, None)

def isPool(poolID):
        """
        Checks if a poolID is valid
        """
        RequestURL = "https://e621.net/pools.json??&search[id]=" + str(poolID)

        # Sends the actual request
        logger.debug(RequestURL)
        eRequest = requests.get(RequestURL, headers=config.e621_headers, auth=(keys.username_e621, keys.api_key_e621))

        # Verify status code
        handleCodes(eRequest.status_code)

        # Checks if there are any results in the response
        if eRequest.json() == []:
            return False
        else:
            return True
