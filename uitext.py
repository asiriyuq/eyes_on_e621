import config

help = """
<b>🔖 Tag Management</b>

/showtags - Shows your tag lists

You can specify multiple tags separated by spaces.

<u>Watched Tags</u>

/addtags <i>tags</i> - Adds tags to your tag watchlist
/remtags <i>tags</i> - Removes tags from your tag watchlist

<u>Incidental Tags</u>

These optional tags are not watched, but will appear in the posts I send you.

/addincs <i>tags</i> - Adds tags to your incidental tags list
/remincs <i>tags</i> - Removes tags from your incidental tags list

<u>Blacklisted Tags</u>

/addbl <i>tags</i> - Adds tags to your blacklist
/rembl <i>tags</i> - Removes tags from your blacklist

<b>📁 Pool Management</b>

/showpools - Shows your pool watchlist

You can specify multiple poolIDs separated by spaces. Note that posts from followed pools will bypass your blacklist!

/addpools <i>poolIDs</i> - Adds poolIDs to your pool watchlist
/rempools <i>poolIDs</i> - Removes poolIDs from your pool watchlist

<b>⚙️ Other</b>

/maxrating - Sets the maximum allowed rating
/faq - Answers frequently asked questions
/testcon - Checks to see if the e621 API is reachable
/stop - Deletes all of your data and resets the bot for you
"""

faq = """
<b>ℹ️ Frequently Asked Questions</b>

<i>Who created you?</i>

@Asiriyuq.

<i>Are you officially affiliated with e621?</i>

No.

<i>How long does it take for posts to show up in my inbox?</i>

I intentionally add a one hour delay so e621 moderators and users have time to correct tagging errors and to remove spam.

<i>I'm receiving posts that I don't want to see! What can I do?</i>

First, use /addbl to blacklist unwanted tags. If a post slips by your blacklist, that's because the person who uploaded it to e621 didn't tag it properly. If the post is spam or inappropriate for e621, it wasn't removed in time before I processed the post.

<i>Can I watch combinations of tags or blacklist some tags only when other tags are present?</i>

No. I was designed to be simple and lightweight, so advanced agent-based searches are not possible.

<i>How secure is my personal data?</i>

My database is encrypted and the password is not stored on my container. My maintainer takes care to keep me up-to-date and regularly reviews my logs. You can choose to instantly delete everything I know about you at any time with /stop.

<i>I want to run this bot myself, maybe even with changes.</i>

Go ahead! My <a href="https://gitlab.com/asiriyuq/eyes_on_e621">[source code]</a> is licensed under the GNU GPLv3.

<i>I have a bug to report or a feature to request.</i>

Raise an issue <a href="https://gitlab.com/asiriyuq/eyes_on_e621/-/issues">[here]</a> or send a message to @Asiriyuq.
"""

start = """
Hi! I'll keep an eye on e621 for you. 👀

👉 Quick start: Tell me what to look out for with /addtags and I'll send you all the matching posts that I see.

<i>I will only send you 🟢 safe posts by default!</i> Use /maxrating to enable questionable or explicit content.

To see what else I can do, type /help.
"""

stop = """
⚠️ To instantly and permanently delete all data associated with you, send /stop_confirm.

/cancel
"""

stop_confirm = """
🚮 I have deleted all of your data. If you want to restart me, send /start. Goodbye!
"""

maxrating = """
<b>🚦 Ratings Management</b>

Send me one of the below commands to change this setting.

🟢 /maxrating_safe - (Default) You will only see safe posts
🟡 /maxrating_questionable - You will see safe and questionable posts
🔴 /maxrating_explicit - You will see all posts including safe, questionable, and explicit posts

See <a href="https://e621.net/help/ratings">here</a> for more details about what these ratings mean.

/cancel
"""

maxrating_success = """
✔️ I have updated your maximum rating preference
"""

oops_no_tags = """
To use this command you must send me at least one tag. You can specify multiple tags separated by spaces.

Usage: /command <i>tag1 tag2 tag3...</i>
"""

oops_no_pools = """
To use this command you must send me at least one pool ID. You can specify multiple pool IDs separated by spaces.

Usage: /command <i>poolID1 poolID2 poolID3...</i>
"""

oops_not_admin = """
❌ Who do you think you are? You're not the boss of me!
"""

oops_stopped = """
❗️ Since you asked me to delete all of your data, I won't be able to process any further commands until you restart me by sending /start!
"""

processing = """
🕐 One moment while I process your command... *busy bot noises*
"""

at_least_one_error = """
\n🚧 I encountered at least one error while processing your request. You can use /testcon to see if the e621 API is down, which happens occasionally. If you are still experiencing issues, contact
"""
at_least_one_error += " " + config.admin_contact + "."

cancel = """
❗️ I've canceled the current action
"""
